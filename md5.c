#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ss.h"
#include <arpa/inet.h>

#define WRITE(offset, value)	(*(volatile uint32_t*)(addr+(offset))) = (value)
#define READ(offset)		(*(uint32_t*)(addr+(offset)))

void *mapmem(void)
{
	int fd;
	void *addr;

	fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x1000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C15000);
	if (addr == MAP_FAILED)
		perror("mmap");

	return addr;
}

void setclock(int state)
{
	int fd;
	void *addr;
	uint32_t clk;

	fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x200, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C20000);
	if (addr == MAP_FAILED)
		perror("mmap");

	clk = READ(0x60);
	clk |= (state << 5);
	WRITE(0x60, clk);

	clk = 0;//READ(0x9C);
	clk |= (state << 31);
	//clk |= (3 << 16);
	//clk |= (15 << 0);
	clk |= (1 << 24); // PLL6
	clk |= (1 << 16); // PLL6/(2^1)/(2+1)
	//clk |= (2 << 0);
	WRITE(0x9C, clk);

	munmap(addr, 0x200);
	close(fd);
}

int main()
{
	void *addr;
	uint32_t flags = 0, interrupt, tmp;
	unsigned char chars[64] = {'p','a','s','s','w','o','r','d',1<<7};
	uint32_t write;
	int i = 0;

	chars[56]=8*8;
	
	printf("Enabling clock...");
	setclock(1);
	printf("   clock enabled!\n");

	printf("Mapping memory...");
	addr = mapmem();
	printf("   memory mapped!\n");

	/* Configure SS for MD5 */
	flags = 0;
	flags |= SUNXI_SS_ENABLED;
	flags |= SUNXI_OP_MD5;
	flags |= SUNXI_IV_CONSTANTS;
	WRITE(SUNXI_SS_CTL, flags);

	printf("IV0 %08x\n", READ(SUNXI_SS_IV0));
	printf("IV1 %08x\n", READ(SUNXI_SS_IV1));
	printf("IV2 %08x\n", READ(SUNXI_SS_IV2));
	printf("IV3 %08x\n", READ(SUNXI_SS_IV3));
	
	//WRITE(SUNXI_SS_IV0, 0x67452301);
	//WRITE(SUNXI_SS_IV1, 0xefcdab89);
	//WRITE(SUNXI_SS_IV2, 0x98badcfe);
	//WRITE(SUNXI_SS_IV3, 0x10325476);

	/* Pass the data to hash */
	while ((tmp=READ(SUNXI_SS_FCSR)) & (1 << 30) && i<64) {
		//if (chars[i] == '\0') break;
		write = (chars[i++] << 0);
		write |= (chars[i++] << 8);
		write |= (chars[i++] << 16);
		write |= (chars[i++] << 24);
		WRITE(SUNXI_SS_RXFIFO, write);
		printf("%08x ", write);
	}
	printf("\n");

	/* Notify that is all the data we have and wait until it finishes */
	WRITE(SUNXI_SS_CTL, flags | SUNXI_DATA_END);
	while (READ(SUNXI_SS_CTL) & SUNXI_DATA_END)
		;

	/* Show hash */
	printf("MD[n]: %08x%08x%08x%08x %08x\n",ntohl(READ(SUNXI_SS_MD0)), ntohl(READ(SUNXI_SS_MD1)), ntohl(READ(SUNXI_SS_MD2)), ntohl(READ(SUNXI_SS_MD3)), ntohl(READ(SUNXI_SS_MD4)));

	/* Shut it down */
	WRITE(SUNXI_SS_CTL, 0);
	setclock(0);

	return 0;
}
