#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ss.h"

#define WRITE(offset, value)	(*(volatile uint32_t*)(addr+(offset))) = (value)
#define READ(offset)		(*(volatile uint32_t*)(addr+(offset)))

void *mapmem(void)
{
	int fd;
	void *addr;

	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x1000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C15000);
	if (addr == MAP_FAILED)
		perror("mmap");

	return addr;
}

void setclock(int state)
{
	int fd;
	void *addr;
	uint32_t clk;

	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x200, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C20000);
	if (addr == MAP_FAILED)
		perror("mmap");

	clk = READ(0x60);
	clk |= (state << 5);
	WRITE(0x60, clk);

	clk = 0;
	clk |= (state << 31);
	clk |= (1 << 24); // PLL6
	clk |= (1 << 16); // PLL6/(2^1)/(2+1)
	clk |= (2 << 0);
	WRITE(0x9C, clk);

	munmap(addr, 0x200);
	close(fd);
}

int main()
{
	void *addr;
	uint32_t flags = 0, reg;
	uint32_t chars = 0;
	int available = 0;

	printf("Enabling clock...");
	setclock(1);
	printf("   clock enabled!\n");

	printf("Mapping memory...");
	addr = mapmem();
	printf("   memory mapped!\n");

	/* Configure SS for PRNG */
	flags |= SUNXI_SS_ENABLED;
	flags |= SUNXI_OP_PRNG;
	flags |= SUNXI_PRNG_CONTINUE;
	WRITE(SUNXI_SS_CTL, flags);

	for(;;) {
		if(available) {
			printf("0x%08x,\n", READ(SUNXI_SS_TXFIFO));
		} else {
			while (!((reg=READ(SUNXI_SS_FCSR)) & SUNXI_TXFIFO_AVAILABLE))
				;
			available = SUNXI_TXFIFO_SPACES(reg);
		}
	}

	/* Shut it down */
	WRITE(SUNXI_SS_CTL, 0);
	setclock(0);

	return 0;
}
