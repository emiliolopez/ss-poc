#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <arpa/inet.h>

#include "ss.h"

#define WRITE(offset, value)	(*(volatile uint32_t*)(addr+(offset))) = (value)
#define READ(offset)		(*(volatile uint32_t*)(addr+(offset)))

void *addr;

void *mapmem(void)
{
	int fd;
	void *addr;

	fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x1000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C15000);
	if (addr == MAP_FAILED)
		perror("mmap");

	return addr;
}

void setclock(int state)
{
	int fd;
	void *addr;
	uint32_t clk;

	fd = open("/dev/mem", O_RDWR|O_SYNC);
	if (!fd)
		perror("open");

	addr = mmap(0, 0x200, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x01C20000);
	if (addr == MAP_FAILED)
		perror("mmap");

	clk = READ(0x60);
	clk |= (state << 5);
	WRITE(0x60, clk);

	clk = 0;//READ(0x9C);
	clk |= (state << 31);
	/*clk |= (3 << 16);
	clk |= (15 << 0);*/
	clk |= (1 << 24); // PLL6
	clk |= (1 << 16); // PLL6/(2^1)/(2+1)
	clk |= (2 << 0);
	WRITE(0x9C, clk);

	munmap(addr, 0x200);
	close(fd);
}

static inline void ss_send(uint32_t value)
{
	static int spaces = 0;
	uint32_t reg;
	
	if (!spaces) {
		while (!((reg=READ(SUNXI_SS_FCSR)) & SUNXI_RXFIFO_FREE))
				;
		spaces = SUNXI_RXFIFO_SPACES(reg);
	}

	WRITE(SUNXI_SS_RXFIFO, value);
	spaces--;
	return;
}

int main()
{
	uint64_t length = 0;
	uint32_t flags = 0, write = 0;
	int c, i = 0, zeros;
	
	printf("Enabling clock...");
	setclock(1);
	printf("   clock enabled!\n");

	printf("Mapping memory...");
	addr = mapmem();
	printf("   memory mapped!\n");

	/* Configure SS for MD5 */
	flags = 0;
	flags |= SUNXI_SS_ENABLED;
	flags |= SUNXI_OP_SHA1;//MD5;
	flags |= SUNXI_IV_CONSTANTS;
	WRITE(SUNXI_SS_CTL, flags);

	/*printf("IV0 %08x\n", READ(SUNXI_SS_IV0));
	printf("IV1 %08x\n", READ(SUNXI_SS_IV1));
	printf("IV2 %08x\n", READ(SUNXI_SS_IV2));
	printf("IV3 %08x\n", READ(SUNXI_SS_IV3));
	printf("CNT0 %08x\n", READ(SUNXI_SS_CNT0));
	
	WRITE(SUNXI_SS_IV0, 0x67452301);
	WRITE(SUNXI_SS_IV1, 0xefcdab89);
	WRITE(SUNXI_SS_IV2, 0x98badcfe);
	WRITE(SUNXI_SS_IV3, 0x10325476);
	WRITE(SUNXI_SS_CNT0, 0xc3d2e1f0);*/

	/* Pass the data to hash */
	while ((c = getchar()) != EOF) {
		write |= (((uint32_t)c) << (i++ * 8));
		if (i == 4) {
			ss_send(write);
			length += 32;
			i = 0;
			write = 0;
		}
	}

	write |= ((1 << 7) << (i * 8));
	ss_send(write);
	
	zeros = (512 - 64 - (length+32) % 512) / 32;
	length += i*8;

	for (i = 0; i < zeros; i++)
		ss_send(0);

	ss_send(length & 0xffffffff);
	ss_send((length >> 32) & 0xffffffff);

	/* Notify that is all the data we have and wait until it finishes */
	WRITE(SUNXI_SS_CTL, flags | SUNXI_DATA_END);
	while (READ(SUNXI_SS_CTL) & SUNXI_DATA_END)
		;

	printf("\n");

	/* Show hash */
	//printf("MD5: %08x%08x%08x%08x %08x\n",ntohl(READ(SUNXI_SS_MD0)), ntohl(READ(SUNXI_SS_MD1)), ntohl(READ(SUNXI_SS_MD2)), ntohl(READ(SUNXI_SS_MD3)), ntohl(READ(SUNXI_SS_MD4)));
	printf("SHA-1: %08x%08x%08x%08x%08x\n", READ(SUNXI_SS_MD0), READ(SUNXI_SS_MD1), READ(SUNXI_SS_MD2), READ(SUNXI_SS_MD3), READ(SUNXI_SS_MD4));

	/* Shut it down */
	WRITE(SUNXI_SS_CTL, 0);
	setclock(0);

	return 0;
}
